module.exports = {
  combine: {
    options: {
      sourceMap: true
    },
    files: {
      'public/css/style.min.css': ['public/css/style.css']
    }
  }
}