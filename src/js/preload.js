class Preload{


	constructor( images, onLoad, onComplete ){
		this.imagesArr = new Array();
		this.progress = 0;
		this.loadCount = 0;
		this.progressStep = 0;
		this.interval;
		this.iTimer = 0;
		this.predictedTime = 0;
		this.onLoad = onLoad;
		this.onComplete = onComplete;
		this.preload( images );
	}


	updateProgress(){
		this.progress ++;
		this.predictedTime = this.iTimer;
		this.iTimer = 0;
		if ( this.onLoad ) this.onLoad( this.progress * this.progressStep, this.predictedTime || .1 );
		if ( this.onComplete && this.progress === this.loadCount){
			this.onComplete( this.imagesArr );
			clearInterval( this.interval );
		}

	}


	preload( inp ){
		for (var i = 0; i < inp.length; i++) {
			this.imagesArr.push( new Array() );
			for (var j = 0; j < inp[i].steps; j++) {
				this.imagesArr[i].push( new Array() );
				this.imagesArr[i][j] = new Image();
				this.imagesArr[i][j].onload = ()=>{ this.updateProgress() };
				this.imagesArr[i][j].steps = inp[i].steps;
				this.imagesArr[i][j].src = `${inp[i].seq}${j+1}${inp[i].ext}`;
				this.loadCount++;
			}
		}
		this.progressStep = 100 / this.loadCount;
		this.interval = setInterval( ()=>{ this.iTimer += 0.1; }, 100 );
	}


}


export default Preload;