module.exports = {
  dist: {
    options: {
      transform: [["babelify"]]
    },
    files: {
      "public/js/app.js": "src/js/app.js"
    }
  }
}