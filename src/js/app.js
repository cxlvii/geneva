import $ from "jquery";
import {TweenMax, TweenlineMax} from "gsap";
import Preload from "./preload";

class App{

  constructor(){

    let that = this;

    this.preloaderTL = new TimelineMax();
    this.TL = new TimelineMax({ paused: true });
    this.finalTL = new TimelineMax({ paused: true });

    this.loadedImages = new Array();
    this.seqInProgress = false;
    this.currentSeq = 0;

    this.DOs = {
      $app: $('.js-app'),
      $imgRef: $('.js-img-ref'),
      $preloader: $('.js-preloader'),
      $preloaderLine: $('.js-preloader-progress'),
      $screenContent: $('.js-screen-content'),
      $mobBtnWrap: $('.js-screen-btn-mob'),
      $plus: $('.js-plus'),
      $pluses: $('.js-pluses>*'),
      $equal: $('.js-equal'),
      $aside: $('.js-aside'),
      $slider: $('.js-slider'),
      $sliderContent: $('.js-slider-content'),
      $asideBg: $('.js-aside-bg'),
      $equalTop: $('.js-equal>*:nth(0)'),
      $equalBot: $('.js-equal>*:nth(1)'),
      $btn: $('.js-btn'),
      $btn_Mob: $('.js-btn-mob'),
      $btnText: $('.js-btn-text'),
      $btnImage: $('.js-btn-image'),
      $btnImage_Mob: $('.js-btn-image-mob')
    };

    this.images = [
      {
        seq: 'i/seq/1/1/',
        ext: '.jpg',
        steps: 3,
        text: 'SURPRISE ME'
      },
      {
        seq: 'i/seq/1/2/',
        ext: '.jpg',
        steps: 22,
        text: 'SHOCK ME'
      },
      {
        seq: 'i/seq/1/3/',
        ext: '.jpg',
        steps: 19,
        text: 'NOW FOR REAL'
      },
      {
        seq: 'i/seq/1/4/',
        steps: 1,
        ext: '.jpg',
        text: "Visit Geneva to be surprised",
        btnImage: 'i/seq/1/btnImg.png'
      }
    ];

    /* Load images before sequences animation */
    this.preloader = new Preload( this.images, ( p, timePrediction ) => {
      // On preloader progress
      this.preloaderTL.add( TweenMax.to( this.DOs.$preloaderLine, timePrediction, {
        width: p + "%"
      }), this.preloaderTL._time);
    }, ( loadedImages ) => {
      // On preloader complete
      this.preloaderTL.add( TweenMax.to( this.DOs.$preloaderLine, .3, {
        height: '100%',
        alpha: .3
      }));
      this.preloaderTL.add( TweenMax.to( this.DOs.$preloader, .5, {
        alpha: 0,
        onComplete: () => {
          /* Animation starts here */
          this.TL.play();
          this.loadedImages = loadedImages;
          this.setImage( this.loadedImages[0][0].src );
        }
      }));
    });


    // Slide
    this.TL.add( TweenMax.fromTo( this.DOs.$screenContent, .4, {
      alpha: 0
    },{
      alpha: 1
    }));

    // Image ref
    this.TL.add( TweenMax.fromTo( this.DOs.$imgRef, .2, {
      y: 90,
      alpha: 0
    },{
      y: 0,
      alpha: 1,
      onComplete: () => {
        /* First sequence start */
        this.startSequence( 0 );
        this.currentSeq ++;
      }
    }));

    // Btn desktop or mobile
    if ( this.bp() === "default" ){
      this.TL.add( TweenMax.fromTo( this.DOs.$btn, .2, {
        y: 90,
        x:'-50%',
        alpha: 0
      },{
        y: 0,
        x: '-50%',
        alpha: 1
      }), '-=.2');
    }else{
      this.TL.add( TweenMax.fromTo( this.DOs.$btn_Mob, .2, {
        y: 90,
        alpha: 0
      },{
        y: 0,
        alpha: 1
      }));
    }


    // Aside bg
    this.TL.add( TweenMax.fromTo( this.DOs.$asideBg, .4, {
      alpha: 0
    },{
      alpha: 1
    }), '-=.2');

    // Plus
    this.TL.add( TweenMax.fromTo( this.DOs.$plus, .3, {
      rotation: -90,
      scale: 0
    },{
      rotation: 0,
      scale: 1
    }), '-=.4');

    // Equal
    this.TL.add( TweenMax.fromTo( this.DOs.$equalTop, .3, {
      transformOrigin: '0% 50%',
      x:'30%',
      scaleX: 0
    },{
      x:'50%',
      scaleX:.5
    }));

    // Equal bottom line
    this.TL.add( TweenMax.fromTo( this.DOs.$equalBot, .3, {
      transformOrigin: '0% 50%',
      x:'30%',
      scaleX: 0
    },{
      x:'50%',
      scaleX: .5
    }));

    // Equal top line
    this.TL.add( TweenMax.to( this.DOs.$equalTop, .3, {
      x:'0%',
      scaleX: 1
    }));

    this.TL.add( TweenMax.to( this.DOs.$equalBot, .3, {
      x:'0%',
      scaleX: 1
    }));

    // Pluses
    this.TL.add( TweenMax.staggerFromTo( this.DOs.$pluses, 1, {
      rotation: 90,
      scale: 0
    },{
      rotation: 0,
      scale: 1,
      ease: Elastic.easeOut
    }, .01 ));


    this.DOs.$btn.on('click', ()=>{
      this.nextSequence();
    });

    this.DOs.$btn_Mob.on('click', ()=>{
      this.nextSequence();
    });


  }

  bp(){
    return ( window.innerWidth <= 768) ? 'mobile' : 'default';
  }

  startSequence( seqNum, callback ){

    this.seqInProgress = true;
    this.currentSeq = seqNum;

    let seq = this.loadedImages[ seqNum ],
        frames = { pos: 0 },
        speed = seq.length * .11,
        currentPos = null;

    TweenMax.to( frames, speed, {
      pos: seq.length - 1,
      ease: SteppedEase.config( seq.length ),
      onUpdate: () => {
        frames.pos = Math.round( frames.pos );
        if ( currentPos !== frames.pos ) {
          currentPos = frames.pos;
          this.setImage( `${seq[currentPos].src}` );
        }
      },
      onComplete: () => {
        this.seqInProgress = false;
        if ( typeof callback === "function" ) { callback(); }
      }
    });

  }

  setImage( imgUrl ){
    this.DOs.$imgRef.attr('src', imgUrl );
  }

  rotatePlus(){

    TweenMax.fromTo( this.DOs.$plus, .3, {
      rotation: -90
    },{
      rotation: 0
    });

  }

  updateBtnText(){
    this.DOs.$btnText.text( this.images[this.currentSeq - 1].text );
  }

  hideBtn(){
    if ( this.bp() === "default" ) {
      this.DOs.$btn.addClass('is-disabled');
      TweenMax.fromTo( this.DOs.$btn, .2, {
        y: 0,
        x:'-50%',
        alpha: 1
      },{
        y: -90,
        x: '-50%',
        alpha: 0,
        onComplete: () => { this.updateBtnText(); }
      });
    } else {
      this.DOs.$btn_Mob.addClass('is-disabled');
      TweenMax.fromTo( this.DOs.$btn_Mob, .2, {
        y: 0,
        alpha: 1
      },{
        y: 90,
        alpha: 0,
        onComplete: () => { this.updateBtnText(); }
      });
    }
  }

  showBtn(){
    if ( this.bp() === "default" ) {
      TweenMax.fromTo( this.DOs.$btn, .2, {
        y: 90,
        x:'-50%',
        alpha: 0
      },{
        y: 0,
        x: '-50%',
        alpha: 1
      });
      this.DOs.$btn.removeClass('is-disabled');
    } else {
      TweenMax.fromTo( this.DOs.$btn_Mob, .2, {
        y: 90,
        alpha: 0
      },{
        y: 0,
        alpha: 1
      });
      this.DOs.$btn_Mob.removeClass('is-disabled');
    }
  }

  runFinalAnimation(){

    let $newPlus = this.DOs.$plus.clone();

    if ( this.bp() === "default" ) {
      this.finalTL.add( TweenMax.to( this.DOs.$equalTop, .2, {
        top:"0"
      }));
      this.finalTL.add( TweenMax.to( this.DOs.$equalBot, .2, {
        top:"auto",
        bottom:"0"
      }), '-=.2');
    } else {
      this.finalTL.add( TweenMax.to( this.DOs.$equalTop, .2, {
        top:"0",
        height: 0,
      }));
      this.finalTL.add( TweenMax.to( this.DOs.$equalBot, .2, {
        top:"auto",
        height: 0,
        bottom:"0"
      }), '-=.2');
    }

    this.finalTL.add( TweenMax.fromTo(this.DOs.$imgRef, .4, {
      alpha: 1,
      scale: 1
    },{
      alpha: 0,
      scale: 0,
      onComplete: ()=> {
        this.fixAppBounds();
        this.DOs.$imgRef.remove();
        TweenMax.set( [ this.DOs.$slider, this.DOs.$sliderContent ], { height:'100%', width:'100%' });
      }
    }),'-=.4');

    this.finalTL.add( TweenMax.to(this.DOs.$aside, .2, {
      alpha: 0,
      width: '0%',
      rotationY: -90,
      x: '-100%',
      onComplete: ()=> {
        this.DOs.$aside.remove();
      }
    }));


    this.finalTL.add( TweenMax.to(this.DOs.$screenContent, .5, {
      width: '100%',
      height: '100%',
      onComplete: ()=> {
        this.DOs.$btnText.text(this.images[this.currentSeq].text);
        if ( this.bp() === "default" ) {
          this.DOs.$btn.addClass('is-black');
          TweenMax.fromTo( this.DOs.$btn, .2, {
            y: 90,
            x:'-50%',
            alpha: 0
          },{
            y: 0,
            x: '-50%',
            alpha: 1,
            onComplete: () => {
              this.showBtnImage();
            }
          });
          this.DOs.$btn.removeClass('is-disabled');
          this.DOs.$btn.off('click');
        } else {
          this.DOs.$btn_Mob.addClass('is-black');
          TweenMax.fromTo( this.DOs.$btn_Mob, .2, {
            y: 90,
            alpha: 0
          },{
            y: 0,
            alpha: 1,
            onComplete: () => {
              this.showBtnImage();
            }
          });
          this.DOs.$btn_Mob.removeClass('is-disabled');
          this.DOs.$btn_Mob.off('click');
        }

        $newPlus.appendTo( this.DOs.$screenContent );
        TweenMax.to( $newPlus, .5, { delay: .3, rotation: 90 });
      }
    }),'-=.2');


    this.finalTL.play();
  }

  showBtnImage(){
    let img = new Image();
        img.src = this.images[ this.currentSeq ].btnImage;

    if ( this.bp() === "default" ) {
      this.DOs.$btnImage.append( img );
      TweenMax.fromTo( img, .6, {
        rotation: -20,
        scale: .4,
        y: 0,
        transformOrigin: '50% 100%'
      },{
        scale: 1,
        rotation: 0,
        y: -30,
        ease: Elastic.easeOut
      });
    } else {
      this.DOs.$btnImage_Mob.append( img );
      TweenMax.fromTo( img, .6, {
        rotation: -20,
        scale: .4,
        y: "0%",
        transformOrigin: '50% 100%'
      },{
        scale: 1,
        rotation: 0,
        y: "-120%",
        ease: Elastic.easeOut
      });
    }


  }


  fixAppBounds(){
    let w = this.DOs.$app.width(),
        h = this.DOs.$app.height();
    TweenMax.set(this.DOs.$app, {width: w, height: h})
  }


  nextSequence(){

    if ( this.currentSeq !== this.loadedImages.length - 1 ){

      // rotate plus
      this.rotatePlus();

      //  hide button
      this.hideBtn();

      this.startSequence( this.currentSeq, ()=>{ this.showBtn() } );
      this.currentSeq ++;

    } else {

      // this.currentSeq ++;
      this.hideBtn();
      this.rotatePlus();
      this.runFinalAnimation();

    }



  }


}


let app = new App();