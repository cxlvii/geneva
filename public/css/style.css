@font-face {
  font-family: "EuclidFlexGeneva";
  font-style: normal;
  font-weight: 500;
  src: url("../fonts/EuclidFlexGeneva-Medium-WebXL.eot?") format("eot"), url("../fonts/EuclidFlexGeneva-Medium-WebXL.woff2") format("woff2"), url("../fonts/EuclidFlexGeneva-Medium-WebXL.woff") format("woff");
}
/*! normalize.css v1.1.3 | MIT License | git.io/normalize */
/* ==========================================================================
   HTML5 display definitions
   ========================================================================== */
/**
 * Correct `block` display not defined in IE 6/7/8/9 and Firefox 3.
 */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
nav,
section,
summary {
  display: block;
}

/**
 * Correct `inline-block` display not defined in IE 6/7/8/9 and Firefox 3.
 */
audio,
canvas,
video {
  display: inline-block;
  *display: inline;
  *zoom: 1;
}

/**
 * Prevent modern browsers from displaying `audio` without controls.
 * Remove excess height in iOS 5 devices.
 */
audio:not([controls]) {
  display: none;
  height: 0;
}

/**
 * Address styling not present in IE 7/8/9, Firefox 3, and Safari 4.
 * Known issue: no IE 6 support.
 */
[hidden] {
  display: none;
}

/* ==========================================================================
   Base
   ========================================================================== */
/**
 * 1. Correct text resizing oddly in IE 6/7 when body `font-size` is set using
 *    `em` units.
 * 2. Prevent iOS text size adjust after orientation change, without disabling
 *    user zoom.
 */
html {
  font-size: 100%;
  /* 1 */
  -ms-text-size-adjust: 100%;
  /* 2 */
  -webkit-text-size-adjust: 100%;
  /* 2 */
}

/**
 * Address `font-family` inconsistency between `textarea` and other form
 * elements.
 */
html,
button,
input,
select,
textarea {
  font-family: sans-serif;
}

/**
 * Address margins handled incorrectly in IE 6/7.
 */
body {
  margin: 0;
}

/* ==========================================================================
   Links
   ========================================================================== */
/**
 * Address `outline` inconsistency between Chrome and other browsers.
 */
a:focus {
  outline: thin dotted;
}

/**
 * Improve readability when focused and also mouse hovered in all browsers.
 */
a:active,
a:hover {
  outline: 0;
}

/* ==========================================================================
   Typography
   ========================================================================== */
/**
 * Address font sizes and margins set differently in IE 6/7.
 * Address font sizes within `section` and `article` in Firefox 4+, Safari 5,
 * and Chrome.
 */
h1 {
  font-size: 2em;
  margin: 0.67em 0;
}

h2 {
  font-size: 1.5em;
  margin: 0.83em 0;
}

h3 {
  font-size: 1.17em;
  margin: 1em 0;
}

h4 {
  font-size: 1em;
  margin: 1.33em 0;
}

h5 {
  font-size: 0.83em;
  margin: 1.67em 0;
}

h6 {
  font-size: 0.67em;
  margin: 2.33em 0;
}

/**
 * Address styling not present in IE 7/8/9, Safari 5, and Chrome.
 */
abbr[title] {
  border-bottom: 1px dotted;
}

/**
 * Address style set to `bolder` in Firefox 3+, Safari 4/5, and Chrome.
 */
b,
strong {
  font-weight: bold;
}

blockquote {
  margin: 1em 40px;
}

/**
 * Address styling not present in Safari 5 and Chrome.
 */
dfn {
  font-style: italic;
}

/**
 * Address differences between Firefox and other browsers.
 * Known issue: no IE 6/7 normalization.
 */
hr {
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  height: 0;
}

/**
 * Address styling not present in IE 6/7/8/9.
 */
mark {
  background: #ff0;
  color: #000;
}

/**
 * Address margins set differently in IE 6/7.
 */
p,
pre {
  margin: 1em 0;
}

/**
 * Correct font family set oddly in IE 6, Safari 4/5, and Chrome.
 */
code,
kbd,
pre,
samp {
  font-family: monospace, serif;
  _font-family: 'courier new', monospace;
  font-size: 1em;
}

/**
 * Improve readability of pre-formatted text in all browsers.
 */
pre {
  white-space: pre;
  white-space: pre-wrap;
  word-wrap: break-word;
}

/**
 * Address CSS quotes not supported in IE 6/7.
 */
q {
  quotes: none;
}

/**
 * Address `quotes` property not supported in Safari 4.
 */
q:before,
q:after {
  content: '';
  content: none;
}

/**
 * Address inconsistent and variable font size in all browsers.
 */
small {
  font-size: 80%;
}

/**
 * Prevent `sub` and `sup` affecting `line-height` in all browsers.
 */
sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sup {
  top: -0.5em;
}

sub {
  bottom: -0.25em;
}

/* ==========================================================================
   Lists
   ========================================================================== */
/**
 * Address margins set differently in IE 6/7.
 */
dl,
menu,
ol,
ul {
  margin: 1em 0;
}

dd {
  margin: 0 0 0 40px;
}

/**
 * Address paddings set differently in IE 6/7.
 */
menu,
ol,
ul {
  padding: 0 0 0 40px;
}

/**
 * Correct list images handled incorrectly in IE 7.
 */
nav ul,
nav ol {
  list-style: none;
  list-style-image: none;
}

/* ==========================================================================
   Embedded content
   ========================================================================== */
/**
 * 1. Remove border when inside `a` element in IE 6/7/8/9 and Firefox 3.
 * 2. Improve image quality when scaled in IE 7.
 */
img {
  border: 0;
  /* 1 */
  -ms-interpolation-mode: bicubic;
  /* 2 */
}

/**
 * Correct overflow displayed oddly in IE 9.
 */
svg:not(:root) {
  overflow: hidden;
}

/* ==========================================================================
   Figures
   ========================================================================== */
/**
 * Address margin not present in IE 6/7/8/9, Safari 5, and Opera 11.
 */
figure {
  margin: 0;
}

/* ==========================================================================
   Forms
   ========================================================================== */
/**
 * Correct margin displayed oddly in IE 6/7.
 */
form {
  margin: 0;
}

/**
 * Define consistent border, margin, and padding.
 */
fieldset {
  border: 1px solid #c0c0c0;
  margin: 0 2px;
  padding: 0.35em 0.625em 0.75em;
}

/**
 * 1. Correct color not being inherited in IE 6/7/8/9.
 * 2. Correct text not wrapping in Firefox 3.
 * 3. Correct alignment displayed oddly in IE 6/7.
 */
legend {
  border: 0;
  /* 1 */
  padding: 0;
  white-space: normal;
  /* 2 */
  *margin-left: -7px;
  /* 3 */
}

/**
 * 1. Correct font size not being inherited in all browsers.
 * 2. Address margins set differently in IE 6/7, Firefox 3+, Safari 5,
 *    and Chrome.
 * 3. Improve appearance and consistency in all browsers.
 */
button,
input,
select,
textarea {
  font-size: 100%;
  /* 1 */
  margin: 0;
  /* 2 */
  vertical-align: baseline;
  /* 3 */
  *vertical-align: middle;
  /* 3 */
}

/**
 * Address Firefox 3+ setting `line-height` on `input` using `!important` in
 * the UA stylesheet.
 */
button,
input {
  line-height: normal;
}

/**
 * Address inconsistent `text-transform` inheritance for `button` and `select`.
 * All other form control elements do not inherit `text-transform` values.
 * Correct `button` style inheritance in Chrome, Safari 5+, and IE 6+.
 * Correct `select` style inheritance in Firefox 4+ and Opera.
 */
button,
select {
  text-transform: none;
}

/**
 * 1. Avoid the WebKit bug in Android 4.0.* where (2) destroys native `audio`
 *    and `video` controls.
 * 2. Correct inability to style clickable `input` types in iOS.
 * 3. Improve usability and consistency of cursor style between image-type
 *    `input` and others.
 * 4. Remove inner spacing in IE 7 without affecting normal text inputs.
 *    Known issue: inner spacing remains in IE 6.
 */
button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  -webkit-appearance: button;
  /* 2 */
  cursor: pointer;
  /* 3 */
  *overflow: visible;
  /* 4 */
}

/**
 * Re-set default cursor for disabled elements.
 */
button[disabled],
html input[disabled] {
  cursor: default;
}

/**
 * 1. Address box sizing set to content-box in IE 8/9.
 * 2. Remove excess padding in IE 8/9.
 * 3. Remove excess padding in IE 7.
 *    Known issue: excess padding remains in IE 6.
 */
input[type="checkbox"],
input[type="radio"] {
  box-sizing: border-box;
  /* 1 */
  padding: 0;
  /* 2 */
  *height: 13px;
  /* 3 */
  *width: 13px;
  /* 3 */
}

/**
 * 1. Address `appearance` set to `searchfield` in Safari 5 and Chrome.
 * 2. Address `box-sizing` set to `border-box` in Safari 5 and Chrome
 *    (include `-moz` to future-proof).
 */
input[type="search"] {
  -webkit-appearance: textfield;
  /* 1 */
  -moz-box-sizing: content-box;
  -webkit-box-sizing: content-box;
  /* 2 */
  box-sizing: content-box;
}

/**
 * Remove inner padding and search cancel button in Safari 5 and Chrome
 * on OS X.
 */
input[type="search"]::-webkit-search-cancel-button,
input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}

/**
 * Remove inner padding and border in Firefox 3+.
 */
button::-moz-focus-inner,
input::-moz-focus-inner {
  border: 0;
  padding: 0;
}

/**
 * 1. Remove default vertical scrollbar in IE 6/7/8/9.
 * 2. Improve readability and alignment in all browsers.
 */
textarea {
  overflow: auto;
  /* 1 */
  vertical-align: top;
  /* 2 */
}

/* ==========================================================================
   Tables
   ========================================================================== */
/**
 * Remove most spacing between table cells.
 */
table {
  border-collapse: collapse;
  border-spacing: 0;
}

html {
  overflow: hidden;
}

*,
*:before,
*:after {
  box-sizing: border-box;
}

body {
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  font-family: EuclidFlexGeneva, sans-serif;
  background: #f50000;
  overflow: hidden;
}

.is-hidden {
  display: none !important;
}

.lyt-screen {
  position: relative;
  width: 1366px;
  height: 768px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-end;
  align-items: center;
  background: #f50000;
  user-select: none;
}
@media (max-width: 1365px) {
  .lyt-screen {
    height: auto;
  }
}
@media (max-width: 768px) {
  .lyt-screen {
    flex-flow: column nowrap;
    justify-content: center;
  }
}
@media (max-width: 629px) {
  .lyt-screen {
    justify-content: flex-start;
  }
}
.lyt-screen-aside {
  height: 100%;
  width: 32%;
}
@media (max-width: 1365px) {
  .lyt-screen-aside {
    position: absolute;
    top: 0;
    left: 0;
  }
}
@media (max-width: 768px) {
  .lyt-screen-aside {
    position: static;
    width: 100%;
    height: auto;
  }
}
.lyt-screen-content {
  width: 68%;
  height: 100%;
  background: #e90101;
}
@media (max-width: 768px) {
  .lyt-screen-content {
    position: relative;
    height: auto;
    width: 100%;
  }
}
.lyt-screen-btn_mob {
  display: none;
  width: 100%;
  padding: 0 30px;
  position: relative;
  z-index: 1;
  height: 20%;
}
@media (max-width: 768px) {
  .lyt-screen-btn_mob {
    display: block;
  }
}

.preloader {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
}
.preloader span {
  display: block;
  width: 0;
  height: 3px;
  background: #fff;
}

.slider {
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  z-index: 1;
}
.slider-content {
  padding: 30px;
  position: relative;
}
@media (max-width: 629px) {
  .slider-content {
    padding: 0 0 30px 0;
  }
}
.slider-content-img {
  width: 100%;
  height: auto;
  box-shadow: 0 45px 100px rgba(0, 0, 0, 0.5);
}
@media (max-width: 629px) {
  .slider-content-img {
    box-shadow: none;
  }
}

.aside {
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: column wrap;
  justify-content: space-between;
  align-items: stretch;
}
.aside img {
  width: 100%;
  height: auto;
}
@media (max-width: 768px) {
  .aside img {
    max-width: 50%;
  }
}
@media (max-width: 768px) {
  .aside {
    height: auto;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: flex-start;
  }
}

.do-plus {
  position: relative;
  width: 100%;
  min-height: 130px;
}
@media (max-width: 1365px) {
  .do-plus {
    position: absolute;
    height: 50%;
    top: 0;
    bottom: 0;
    margin: auto;
    flex: 0;
    overflow: hidden;
  }
}
@media (max-width: 768px) {
  .do-plus {
    position: absolute;
    min-height: auto;
    height: auto;
  }
}
.do-plus-bg {
  display: flex;
  flex-flow: row wrap;
  opacity: .4;
  justify-content: space-between;
  align-items: center;
}
.do-plus-bg > * {
  margin: 18px 19px;
}
@media (max-width: 1365px) {
  .do-plus-bg {
    height: 100%;
    justify-content: center;
  }
}
.do-plus-sprite {
  position: relative;
  width: 9px;
  height: 9px;
}
.do-plus-sprite:before, .do-plus-sprite:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 3px;
  height: 100%;
  background: #fff;
}
.do-plus-sprite:after {
  width: 100%;
  height: 3px;
}
.do-plus-sprite.is-main {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 100px;
  height: 100px;
}
.do-plus-sprite.is-main:before {
  width: 33px;
}
.do-plus-sprite.is-main:after {
  height: 33px;
}
@media (max-width: 629px) {
  .do-plus-sprite.is-main {
    width: 50px;
    height: 50px;
  }
  .do-plus-sprite.is-main:before {
    width: 15px;
  }
  .do-plus-sprite.is-main:after {
    height: 15px;
  }
}
.do-equal {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
}
.do-equal .do-equal-line {
  position: absolute;
  width: 100%;
  height: 33px;
  right: 0;
  background: #fff;
}
.do-equal .do-equal-line.is-top {
  top: calc(50% - 50px);
}
.do-equal .do-equal-line.is-bottom {
  bottom: calc(50% - 50px);
}

.btn {
  position: absolute;
  display: inline-block;
  white-space: nowrap;
  left: 50%;
  bottom: 80px;
  height: 70px;
  color: #222;
  margin: auto;
  padding: 25px 60px;
  background: #fff;
  font-size: 17px;
  letter-spacing: 1px;
  margin-left: 0;
  margin-top: 0;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
  cursor: pointer;
  transform: translateX(-50%);
  transition: 0.2s margin, 0.2s bottom;
}
@media (max-width: 1365px) {
  .btn {
    height: 50px;
    padding: 15px 50px;
  }
}
@media (max-width: 768px) {
  .btn {
    display: none;
  }
}
.btn-image {
  position: absolute;
  left: 50%;
  bottom: 80px;
  transform: translateX(-50%);
  z-index: -1;
}
.btn:before, .btn:after {
  content: '';
  display: block;
  position: absolute;
  right: -9px;
  top: 0;
  width: 10px;
  height: 70px;
  background: #e3e3e3;
  transform-origin: 0%  0%;
  transform: skewY(45deg);
  transition: 0.2s width, 0.2s height, 0.2s right, 0.2s bottom;
}
@media (max-width: 1365px) {
  .btn:before, .btn:after {
    height: 50px;
  }
}
.btn:after {
  right: 0;
  top: auto;
  bottom: -9px;
  width: 100%;
  height: 10px;
  background: #bfbfbf;
  transform-origin: 0% 0%;
  transform: skewX(45deg);
}
.btn:active {
  margin-left: 10px;
  margin-top: 10px;
  bottom: 70px;
}
.btn:active:before {
  width: 0;
  right: 0;
}
.btn:active:after {
  height: 0;
  bottom: 0;
}
.btn.is-black {
  color: #fff;
  background: #3e3e3f;
}
.btn.is-black:before {
  background: #363637;
}
.btn.is-black:after {
  background: #262628;
}
.btn.is-disabled {
  pointer-events: none;
}

.btn_mob {
  position: relative;
  top: 0;
  left: 0;
  height: 70px;
  color: #222;
  margin: auto;
  padding: 25px 60px;
  background: #fff;
  font-size: 17px;
  letter-spacing: 1px;
  margin-left: 0;
  margin-top: 0;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
  cursor: pointer;
  transition: 0.2s top, 0.2s left;
  z-index: 1;
}
@media (max-width: 768px) {
  .btn_mob {
    padding: 15px 30px;
    font-size: 17px;
    height: auto;
  }
}
.btn_mob:before, .btn_mob:after {
  content: '';
  display: block;
  position: absolute;
  right: -9px;
  top: 0;
  width: 10px;
  height: 100%;
  background: #e3e3e3;
  transform-origin: 0%  0%;
  transform: skewY(45deg);
  transition: 0.2s width, 0.2s height, 0.2s right, 0.2s bottom;
}
.btn_mob:after {
  right: 0;
  top: auto;
  bottom: -9px;
  width: 100%;
  height: 10px;
  background: #bfbfbf;
  transform-origin: 0% 0%;
  transform: skewX(45deg);
}
.btn_mob:active {
  left: 10px;
  top: 10px;
}
.btn_mob:active:before {
  width: 0;
  right: 0;
}
.btn_mob:active:after {
  height: 0;
  bottom: 0;
}
.btn_mob.is-black {
  color: #fff;
  background: #3e3e3f;
}
.btn_mob.is-black:before {
  background: #363637;
}
.btn_mob.is-black:after {
  background: #262628;
}
.btn_mob.is-disabled {
  pointer-events: none;
}

.btn-image_mob {
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  z-index: -1;
}

/*# sourceMappingURL=style.css.map */
