module.exports = {
  images: {
    expand: true,
    cwd: 'src/i/',
    src: ['**/*.{png,jpg,svg}'],
    dest: 'public/i/'
  },
  font: {
    expand: true,
    cwd: 'src/fonts/',
    src: ['**/*.*'],
    dest: 'public/fonts/'
  }
}